<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Post
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @UniqueEntity("name")
 * @Gedmo\Tree(type="nested")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Categories have One Category.
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @Gedmo\TreeParent
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     */
    protected $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    // ==================================================================

    /**
     * @var integer
     *
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $treeLeft;
    /**
     * @var integer
     *
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $treeLevel;
    /**
     * @var integer
     *
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $treeRight;


    /**
     * One Category has Many Categories.
     *
     * @var Category[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     * @ORM\OrderBy({"treeLeft" = "ASC"})
     */
    protected $children;

    /**
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="categories")
     */
    protected $posts;

    // ==================================================================

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Category $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Category[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Category[] $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param int $level
     */
    public function setTreeLevel($level)
    {
        $this->treeLevel = $level;
    }
    /**
     * @return int
     */
    public function getTreeLevel()
    {
        return $this->treeLevel;
    }

    /**
     * @param int $treeLeft
     */
    public function setTreeLeft($treeLeft)
    {
        $this->treeLeft = $treeLeft;
    }
    /**
     * @return int
     */
    public function getTreeLeft()
    {
        return $this->treeLeft;
    }
    /**
     * @param int $treeRight
     */
    public function setTreeRight($treeRight)
    {
        $this->treeRight = $treeRight;
    }
    /**
     * @return int
     */
    public function getTreeRight()
    {
        return $this->treeRight;
    }

    public function getSlug()
    {
        return $this->slug;
    }
}
