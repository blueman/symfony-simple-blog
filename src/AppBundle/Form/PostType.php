<?php
namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\Post;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Yavin\Symfony\Form\Type\TreeType;

/**
 * Class PostType.
 *
 * @package AppBundle\Form
 */
class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'Post title'])
            ->add('categories', TreeType::class, [
                'label' => 'Categories',
                'choice_label' => 'name',
                'class' => Category::class,
                'required' => false,
                'multiple' => true,
                'levelPrefix' => '--- ',
            ])
            ->add('body', null, ['label' => 'Post body'])
            ->add('visible', null, ['label' => 'Visible?'])
        ;
    }

    /**
     * {@inheritdoc}
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Post::class,
        ));
    }
}
