<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/admin/comment")
 */
class CommentController extends Controller
{
    /**
     * @Route("/", name="admin_comment_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $commentsDb = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findAllByOrder();

        $paginator  = $this->get('knp_paginator');
        $comments = $paginator->paginate(
            $commentsDb,
            $request->query->getInt('page', 1)
        );


        return $this->render('comment/index.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/show/{id}", name="admin_comment_show", options={"expose"=true})
     * @ParamConverter("comment", class="AppBundle:Comment")
     */
    public function showAction(Comment $comment)
    {
        return new JsonResponse(['comment' => $comment], 200);
    }

    /**
     * @Route("/delete/{id}", name="admin_comment_delete")
     * @ParamConverter("comment", class="AppBundle:Comment")
     */
    public function deleteAction(Comment $comment)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();

        $this->addFlash('success', 'Comment deleted.');

        return $this->redirectToRoute('admin_comment_index');
    }
}
