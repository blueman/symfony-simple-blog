<?php

namespace AppBundle\Services;

use Cocur\Slugify\Slugify;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class UploadFile.
 *
 * @package AppBundle\Services
 */
class UploadFile
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var string
     */
    private $mediaUploadDir;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var string
     */
    private $fullFileName;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * UploadFile constructor.
     *
     * @param ValidatorInterface $validator
     * @param string $mediaUploadDir
     * @param SessionInterface $session
     */
    public function __construct(ValidatorInterface $validator, $mediaUploadDir, SessionInterface $session)
    {
        $this->validator = $validator;
        $this->mediaUploadDir = $mediaUploadDir;
        $this->session = $session;
    }

    /**
     * Main function - should be call first.
     *
     * @param UploadedFile $file
     *
     * @return bool
     */
    public function upload(UploadedFile $file)
    {
        $this->file = $file;
        $validate = $this->validate();

        if (count($validate) == 0) {
            $fileOriginalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            $this->generateFullFileName($fileOriginalName);

            $file->move($this->mediaUploadDir, $this->fullFileName);

            /**
             * Store the file in a session - as an array of files stored in session.
             */
            $temporaryFiles = $this->session->get('temporaryFiles', []);
            $temporaryFiles[] = $this->fullFileName;
            $this->session->set('temporaryFiles', $temporaryFiles);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Generate unique file name - based on original name and similar files currently stored in storage.
     *
     * @param string $fileOriginalName
     */
    private function generateFullFileName($fileOriginalName)
    {
        /**
         * Make a file name unique.
         */
        $slugify = new Slugify();
        $fileName = $slugify->slugify($fileOriginalName).'-'.time();
        $this->fullFileName = $fileName.'.'.$this->file->guessExtension();

        $fs = new Filesystem();
        if ($fs->exists($this->mediaUploadDir.'/'.$this->fullFileName)) {
            /**
             * If the file exists - add a number at the end of a file name.
             */
            $i = 0;
            do {
                $fileName = $slugify->slugify($fileOriginalName).'-'.time().'-'.++$i;
                $this->fullFileName = $fileName.'.'.$this->file->guessExtension();
            } while ($fs->exists($this->mediaUploadDir.'/'.$this->fullFileName));
        }
    }

    /**
     * Get generated file name stored in a storage.
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fullFileName;
    }

    /**
     * Get list of errors of validation.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Validate the uploaded files against some rules.
     *
     * @return array
     */
    private function validate()
    {
        $violations = $this->validator->validate($this->file, [
            new Image(['detectCorrupted' => true])
        ]);

        $this->errors = [];

        if ($violations->count() > 0) {
            // errors
            foreach ($violations as $violation) {
                $this->errors[] = $violation->getMessage();
            }
        }

        return $this->errors;
    }
}
