<?php

namespace AppBundle\Tests;

class PHPLintTest extends \PHPUnit_Framework_TestCase
{
    private function lint($dir)
    {
        foreach (new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS|\FilesystemIterator::UNIX_PATHS) as $path => $objSplFileInfo) {
            // recurse if dir
            if ($objSplFileInfo->isDir()) {
                $this->lint($objSplFileInfo->getPathName());
                continue;
            }

            // are there any non-dirs that aren't files?
            if (!$objSplFileInfo->isFile()) {
                // throw new \UnexpectedValueException(sprintf('Not a dir and not a file (`%s`)?',$objSplFileInfo->getPathName()));
            }
            // skip non-php files
            if (preg_match('#\.php$#', $objSplFileInfo->getFileName()) !== 1) {
                continue;
            }
            // perform the lint check
            $result = exec('php -l ' . escapeshellarg($objSplFileInfo));
            if (preg_match('#^No syntax errors detected in#', $result) !== 1) {
                $this->fail(sprintf('Syntax errors detected in `%s`', $objSplFileInfo->getPathName()));
            }
        }
    }

    /**
     * @group lint
     */
    public function testAppBundle()
    {
        $this->lint(__DIR__.'/..');
    }
}
