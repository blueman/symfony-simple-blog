<?php

namespace AppBundle\Repository;

/**
 * PostRepository
 */
class PostRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get visible posts.
     *
     * @return array
     */
    public function visiblePosts()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p')
            ->where('p.visible = :visible')
            ->orderBy('p.createdAt', 'desc')
            ->setParameters([
                'visible' => true,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * Search posts by category or text of post.
     *
     * @param string|null $categoryslug
     * @param string|null $text
     *
     * @return array
     */
    public function searchPosts($categoryslug = null, $text = null)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p')
            ->where('p.visible = :visible')
            ->orderBy('p.createdAt', 'desc')
            ->setParameter('visible', true);

        if (!is_null($categoryslug)) {
            $qb->innerJoin('p.categories', 'c')
                ->andWhere('c.slug = :category')
                ->setParameter('category', $categoryslug)
            ;
        }

        if (!is_null($text)) {
            $qb->andWhere('p.body LIKE :text')
                ->setParameter('text', '%'.$text.'%');
        }

        return $qb->getQuery()->getResult();
    }
}
