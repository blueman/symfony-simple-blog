<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/new", name="admin_category_new")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse([
                'message' => 'You can access this only using Ajax!',
            ], 400);
        }

        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return new JsonResponse(['message' => 'Success!'], 200);
        }

        return new JsonResponse([
            'message' => 'Error',
            'form' => $this->renderView('::category/form.html.twig', [
                'categoryForm' => $form->createView(),
            ])
        ], 400);
    }

    /**
     * @Route("/edit/{id}", name="admin_category_edit", options={"expose"=true})
     * @ParamConverter("category", class="AppBundle:Category")
     * @Method("POST")
     */
    public function editAction(Category $category, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse([
                'message' => 'You can access this only using Ajax!',
            ], 400);
        }

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $category = $form->getData();

            $em->persist($category);
            $em->flush();

            return new JsonResponse(['message' => 'Success!'], 200);
        }

        return new JsonResponse([
            'message' => 'Error',
            'form' => $this->renderView('::category/form.html.twig', [
                'categoryForm' => $form->createView(),
            ])
        ], 400);
    }

    /**
     * @Route("/delete/{id}", name="admin_category_delete")
     * @ParamConverter("category", class="AppBundle:Category")
     */
    public function deleteAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        $this->addFlash('success', 'Category deleted.');

        return $this->redirectToRoute('admin_post_index');
    }
}
