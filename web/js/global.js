$('select[multiple]').on('mousedown', 'option', function(e) {
    e.preventDefault();
    $(this).prop('selected', !$(this).prop('selected'));
});