<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Media;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/media")
 */
class MediaController extends Controller
{
    /**
     * @Route("/", name="admin_media_list")
     */
    public function indexAction(Request $request)
    {
        $mediaDb = $this->getDoctrine()
            ->getRepository('AppBundle:Media')
            ->findAll();

        $paginator  = $this->get('knp_paginator');
        $media = $paginator->paginate(
            $mediaDb, /* query NOT result */
            $request->query->getInt('page', 1) /*page number*/
        );

        return $this->render('media/index.html.twig', [
            'medias' => $media,
        ]);
    }

    /**
     * @Route("/upload", name="admin_media_upload")
     */
    public function uploadAction(Request $request)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        $uploadingFile = $this->get('app.upload_file');

        $jsonResponse = $uploadingFile->getErrors();

        if ($uploadingFile->upload($file)) {
            $jsonResponse = [
                'files' => [[
                    'name' => $uploadingFile->getFileName(),
                    'size' => $file->getClientSize(),
                    'thumbnailUrl' => $this->get('assets.packages')->getUrl('media/temp/'.$uploadingFile->getFileName()),
                    'type' => 'image/jpeg',
                    'url' => $this->get('assets.packages')->getUrl('media/temp/'.$uploadingFile->getFileName()),
                    'deleteUrl' => $this->generateUrl('admin_media_remove_session', ['file' => $uploadingFile->getFileName()]),
                    'deleteType' => 'POST'
                ]],
            ];
        }

        return new JsonResponse($jsonResponse);
    }

    /**
     * @Route("/session-remove/{file}", name="admin_media_remove_session")
     */
    public function sessionRemoveAction($file, Request $request)
    {
        $deleteFileService = $this->get('app.delete_file_session');
        $deleteFileService->deleteSessionedFile($file);

        if($request->isXmlHttpRequest()) {
            return new JsonResponse([$file => true]);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/delete/{id}", name="admin_media_remove")
     * @ParamConverter("media", class="AppBundle:Media")
     */
    public function deleteAction(Media $media, Request $request)
    {
        $deleteFileService = $this->get('app.delete_file_storage');
        $deleteFileService->deleteStoredFile($media);

        $this->addFlash('success', 'Media deleted.');

        return $this->redirect($request->headers->get('referer'));
    }
}
