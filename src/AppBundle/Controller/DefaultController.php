<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class DefaultController.
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $postsDb = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->visiblePosts();

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $postsDb,
            $request->query->getInt('page', 1)
        );

        return $this->render('default/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/show/{id}", name="post_show")
     * @ParamConverter("post", class="AppBundle:Post")
     */
    public function showAction(Post $post, Request $request)
    {
        if (!$post->isVisible() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        $commentsDb = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->getByPost($post);

        $paginator  = $this->get('knp_paginator');
        $comments = $paginator->paginate(
            $commentsDb,
            $request->query->getInt('page', 1),
            25
        );

        $comment = new Comment();
        $comment->setPost($post);

        $commentForm = $this->createForm(CommentType::class, $comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $comment = $commentForm->getData();
            $comment->setUser($this->getUser());

            $em->persist($comment);
            $em->flush();

            $this->addFlash('success', 'Comment added');

            return $this->redirectToRoute('post_show', ['id' => $post->getId()]);
        }

        return $this->render('default/show.html.twig', [
            'post' => $post,
            'comments' => $comments,
            'commentForm' => $commentForm->createView(),
        ]);
    }

    /**
     * @Route("/search/{categorySlug}", name="search")
     */
    public function searchAction($categorySlug = null, Request $request)
    {
        $postsDb = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->searchPosts($categorySlug, $request->get('search'));

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $postsDb,
            $request->query->getInt('page', 1)
        );

        return $this->render('default/index.html.twig', [
            'posts' => $posts,
            'searchText' => $request->get('search'),
        ]);
    }

    /**
     * @param $requestAttributes
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sidebarAction($requestAttributes)
    {
        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findParents();

        return $this->render('default/sidebar.html.twig', [
            'categories' => $categories,
        ]);
    }
}
