<?php
namespace AppBundle\Twig;

/**
 * Class AppExtension.
 *
 * @package AppBundle\Twig
 */
class AppExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('format_bytes', [$this, 'format_bytes']),
        );
    }

    /**
     * Return human readable format of bytes.
     *
     * @param int $bytes
     *
     * @return string
     */
    public function format_bytes($bytes)
    {
        $unit = 1024;
        if ($bytes <= $unit) {
            return $bytes . " B";
        }
        $exp = intval((log($bytes) / log($unit)));
        $pre = "KMGTPE";
        $pre = $pre[$exp - 1];
        return sprintf("%.2f %sB", $bytes / pow($unit, $exp), $pre);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'app_extension';
    }
}
