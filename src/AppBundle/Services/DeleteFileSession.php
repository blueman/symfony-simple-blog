<?php

namespace AppBundle\Services;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class DeleteFileSession.
 *
 * @package AppBundle\Services
 */
class DeleteFileSession
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var string
     */
    private $mediaUploadDir;

    /**
     * DeleteFileSession constructor.
     *
     * @param SessionInterface $session
     * @param string $mediaUploadDir
     */
    public function __construct(SessionInterface $session, $mediaUploadDir)
    {
        $this->session = $session;
        $this->mediaUploadDir = $mediaUploadDir;
    }

    /**
     * Deleting file stored in a session and on a storage.
     *
     * @param string $file
     */
    public function deleteSessionedFile($file)
    {
        $fs = new Filesystem();
        $fs->remove($this->mediaUploadDir.'/'.$file);

        $temporaryFiles = $this->session->get('temporaryFiles', []);

        for ($i = 0; $i < count($temporaryFiles); $i++) {
            if ($temporaryFiles[$i] == $file) {
                unset($temporaryFiles[$i]);
            }
        }
        $this->session->set('temporaryFiles', array_values($temporaryFiles));
    }
}
