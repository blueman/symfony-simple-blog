<?php

namespace AppBundle\Repository;

/**
 * CategoryRepository
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get just category parents.
     *
     * @return array
     */
    public function findParents()
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('c')
            ->where('c.parent IS NULL')
            ->orderBy('c.name', 'asc')
        ;

        return $qb->getQuery()->getResult();
    }
}
