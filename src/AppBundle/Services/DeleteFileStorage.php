<?php

namespace AppBundle\Services;

use AppBundle\Entity\Media;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

/**
 * Class DeleteFileStorage.
 *
 * @package AppBundle\Services
 */
class DeleteFileStorage
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * DeleteFileStorage constructor.
     *
     * @param EntityManagerInterface $em
     * @param KernelInterface $kernel
     * @param CacheManager $cacheManager
     */
    public function __construct(EntityManagerInterface $em, KernelInterface $kernel, CacheManager $cacheManager)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->cacheManager = $cacheManager;
    }

    /**
     * Deleting file stored in a database and on a storage.
     *
     * @param Media $media
     */
    public function deleteStoredFile(Media $media)
    {
        $this->em->remove($media);
        $this->em->flush();

        $webDir = $this->kernel->getRootDir().'/../web';
        $pngPath = $media->getPath().'/'.$media->getFile();

        $fs = new Filesystem();
        $fs->remove($webDir.'/'.$pngPath);

        $this->cacheManager->resolve($pngPath, 'standard');
        $this->cacheManager->remove($pngPath);
    }
}
