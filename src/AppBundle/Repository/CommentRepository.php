<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Post;

/**
 * CommentRepository
 */
class CommentRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get comments assignee to a post.
     *
     * @param Post $post
     *
     * @return array
     */
    public function getByPost(Post $post)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('c')
            ->where('c.post = :post')
            ->orderBy('c.createdAt', 'desc')
            ->setParameters([
                'post' => $post,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all comments ordered by a createdAt.
     *
     * @param string $order
     *
     * @return array
     */
    public function findAllByOrder($order = 'desc')
    {
        if (!in_array(strtolower($order), ['asc', 'desc'])) {
            throw new \InvalidArgumentException();
        }

        $qb = $this->createQueryBuilder('c');
        $qb->select('c')->orderBy('c.createdAt', $order);

        return $qb->getQuery()->getResult();
    }
}
