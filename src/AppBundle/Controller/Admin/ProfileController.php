<?php

namespace AppBundle\Controller\Admin;

use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/admin/profile")
 */
class ProfileController extends Controller
{
    /**
     * @Route("/", name="profile")
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /**
         * @var $formFactory FactoryInterface
         */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var $userManager UserManagerInterface
             */
            $userManager = $this->get('fos_user.user_manager');

            $userManager->updateUser($user);

            $this->addFlash('success', 'Your changes were saved!');

            $url = $this->generateUrl('profile');
            $response = new RedirectResponse($url);

            return $response;
        }

        return $this->render('profile/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
