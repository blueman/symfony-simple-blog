<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class LoginPageListener.
 * Listener responsible to change the redirection at the login form.
 *
 * @package AppBundle\EventListener
 */
class LoginPageListener implements EventSubscriberInterface
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * LoginPageListener constructor.
     *
     * @param Router $router
     * @param SessionInterface $session
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(Router $router, SessionInterface $session, TokenStorageInterface $tokenStorage)
    {
        $this->router = $router;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    /**
     * Kernel event listener.
     *
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /**
         * Prevent display login page for authenticated users.
         */
        if (
            $controller[0] instanceof \FOS\UserBundle\Controller\SecurityController
            && $controller[1] == 'loginAction'
            && !is_null($this->tokenStorage->getToken())
            && $this->tokenStorage->getToken()->getUser() instanceof User
        ) {
            $this->session->getFlashBag()->add('success', 'You are still login.');
            $redirectUrl= $this->router->generate('dashboard');

            /**
             * Make the final redirection.
             */
            $event->setController(function () use ($redirectUrl) {
                return new RedirectResponse($redirectUrl);
            });
        }
    }
}
