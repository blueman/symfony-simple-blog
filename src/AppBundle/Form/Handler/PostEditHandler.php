<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Post;
use AppBundle\Entity\Media;
use Symfony\Component\Asset\Packages;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class PostEditHandler.
 *
 * @package AppBundle\Form\Handler
 */
class PostEditHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var string
     */
    private $mediaUploadTempDir;

    /**
     * @var string
     */
    private $mediaUploadDir;

    /**
     * @var Packages
     */
    private $assetsPackages;

    /**
     * PostEditHandler constructor.
     *
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     * @param string $mediaUploadTempDir
     * @param string $mediaUploadDir
     * @param Packages $assetsPackages
     */
    public function __construct(EntityManagerInterface $em, SessionInterface $session, $mediaUploadTempDir, $mediaUploadDir, Packages $assetsPackages)
    {
        $this->em = $em;
        $this->session = $session;
        $this->mediaUploadTempDir = $mediaUploadTempDir;
        $this->mediaUploadDir = $mediaUploadDir;
        $this->assetsPackages = $assetsPackages;
    }

    /**
     * Handle the form request - with passed $post entity variable.
     *
     * @param Post $post
     */
    public function handleRequest(Post $post)
    {
        $temporaryFiles = $this->session->get('temporaryFiles', []);
        $uploadDir = $this->mediaUploadDir.'/'.date('Y/m');

        foreach ($temporaryFiles as $key => $temporaryFile) {
            $fullFileName = $this->generateFullFileName($uploadDir, $temporaryFile);

            $file = new File($uploadDir.'/'.$fullFileName);

            $media = new Media();
            $media->setPost($post);
            $media->setPath($this->assetsPackages->getUrl('media/uploads/'.date('Y/m')));
            $media->setFile($fullFileName);
            $media->setType($file->getMimeType());
            $media->setSize($file->getSize());

            $this->em->persist($media);

            unset($temporaryFiles[$key]);
        }

        $this->session->set('temporaryFiles', $temporaryFiles);

        $this->em->persist($post);
        $this->em->flush();
    }

    /**
     * Generate unique file name - based on original name and similar files currently stored in storage.
     *
     * @param string $uploadDir
     * @param string $temporaryFile
     *
     * @return string
     */
    private function generateFullFileName($uploadDir, $temporaryFile)
    {
        $fs = new Filesystem();
        $fs->mkdir($uploadDir);

        $fullFileName = $temporaryFile;
        if ($fs->exists($uploadDir.'/'.$temporaryFile)) {
            $i = 0;
            do {
                $fileName = pathinfo($temporaryFile, PATHINFO_FILENAME).'-'.++$i;
                $fullFileName = $fileName.'.'.pathinfo($temporaryFile, PATHINFO_EXTENSION);
            } while ($fs->exists($uploadDir.'/'.$fullFileName));
        }

        $fs->rename(
            $this->mediaUploadTempDir.'/'.$temporaryFile,
            $uploadDir.'/'.$fullFileName
        );

        return $fullFileName;
    }
}
