<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/admin/post")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="admin_post_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $postsDb = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $postsDb, /* query NOT result */
            $request->query->getInt('page', 1) /*page number*/
        );

        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findParents();

        $categoryForm = $this->createForm(CategoryType::class, new Category());
        $categoryEditForm = $this->createForm(CategoryType::class, new Category());

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
            'categories' => $categories,
            'categoryForm' => $categoryForm->createView(),
            'categoryEditForm' => $categoryEditForm->createView(),
        ]);
    }

    /**
     * @Route("/add", name="admin_post_new")
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $post = $form->getData();
            $post->setAuthor($this->getUser());

            $em->persist($post);
            $em->flush();

            $this->addFlash('success', 'Post created.');

            return $this->redirectToRoute('admin_post_index');
        }

        //$this->get('session')->set('temporaryFiles', []);

        return $this->render('post/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_post_edit")
     * @ParamConverter("post", class="AppBundle:Post")
     */
    public function editAction(Post $post, Request $request)
    {
        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.post_edit_form_handler')->handleRequest($form->getData());

            $this->addFlash('success', 'Post edited.');

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_post_delete")
     * @ParamConverter("post", class="AppBundle:Post")
     */
    public function deleteAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        $this->addFlash('success', 'Post deleted.');

        return $this->redirectToRoute('admin_post_index');
    }
}
